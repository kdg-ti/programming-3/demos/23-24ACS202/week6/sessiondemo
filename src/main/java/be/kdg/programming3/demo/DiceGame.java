package be.kdg.programming3.demo;

import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import java.util.Random;

@Component
@SessionScope
public class DiceGame {
    private int score;
    private int count;

    public int getScore() {
        return score;
    }

    public int getCount() {
        return count;
    }

    public void throwDice(){
        Random random = new Random();
        int die1 = random.nextInt(1,7);
        int die2 = random.nextInt(1,7);
        this.score += die1 + die2;
        this.count++;
    }
}
