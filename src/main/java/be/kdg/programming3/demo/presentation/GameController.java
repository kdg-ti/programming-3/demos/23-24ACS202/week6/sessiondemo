package be.kdg.programming3.demo.presentation;

import be.kdg.programming3.demo.DiceGame;
import jakarta.servlet.http.HttpSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GameController {
    private static final Logger logger = LoggerFactory.getLogger(GameController.class);
   // @Autowired
   // private DiceGame diceGame;

    @GetMapping("/")
    public String getHomeView(){
        logger.info("Getting Home View");
        return "index";
    }
    @PostMapping("/")
    public String throwTheDice(Model model, HttpSession session){
        logger.info("Throwing the dice....");
        DiceGame diceGame = (DiceGame) session.getAttribute("diceGame");
        if (diceGame==null) {
            diceGame = new DiceGame();
            session.setAttribute("diceGame", diceGame);
        }
        diceGame.throwDice();
        if (diceGame.getCount()==3) {
            session.invalidate();//will throw away the session!
        }
        model.addAttribute("diceGame", diceGame);
        return "index";
    }
//    @PostMapping("/")
//    public String throwTheDice(Model model){
//        logger.info("Throwing the dice....");
//        diceGame.throwDice();
//        model.addAttribute("diceGame", diceGame);
//        return "index";
//    }
}
